
// @ts-ignore
import { serve } from 'https://deno.land/std/http/server.ts';
// @ts-ignore
import { readFileStr } from 'https://deno.land/std/fs/mod.ts';

const listener = serve("0.0.0.0:8080");
const indexFile = readFileStr('./www/index.html', { encoding: 'utf8' });

const app = ( async () => {
    console.log( 'server started on 8080 port, open http://localhost:80000 in your browser, please' );
    for await (const req of listener) {
        req.respond(
            {
                body:
                    new TextEncoder().encode( await indexFile)
            });
    }
})();


